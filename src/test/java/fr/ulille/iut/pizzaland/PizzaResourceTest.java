package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import fr.ulille.iut.pizzaland.dto.PizzaDto;

/*
 * JerseyTest facilite l'écriture des tests en donnant accès aux
 * méthodes de l'interface javax.ws.rs.client.Client.
 * la méthode configure() permet de démarrer la ressource à tester
 */
public class PizzaResourceTest extends JerseyTest {

        @Override
        protected Application configure() {
                BDDFactory.setJdbiForTests();

                return new ApiV1();
        }

           
        
        @Test
        public void testGetEmptyList() {
                Response response = target("/pizzas").request().get();
                
                assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

                List<PizzaDto> pizza;
                pizza = response.readEntity(new GenericType<List<PizzaDto>>(){});

                assertEquals(0, pizza.size());

        }
        
}
