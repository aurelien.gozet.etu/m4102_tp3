package fr.ulille.iut.pizzaland.dao;

import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

public interface PizzaDao {

  @SqlUpdate("DROP TABLE IF EXISTS Pizzas")
  void dropPizzaTable();

  @SqlUpdate("DROP TABLE IF EXISTS PizzaIngredientsAssociation")
  void dropAssociationTable();


  @SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas (id INTEGER PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
  void createPizzaTable();

  @SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (idPizza INTEGER PRIMARY KEY, idIngredient INTEGER PRIMARY KEY)")
  void createAssociationTable();

  @SqlUpdate("INSERT INTO pizzas (name) VALUES (:name)")
  @GetGeneratedKeys
  long insert(String name);

  @Transaction
  default void createTableAndIngredientAssociation() {
    createAssociationTable();
    createPizzaTable();
  }
}